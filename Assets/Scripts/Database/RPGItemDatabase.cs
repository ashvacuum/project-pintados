﻿using RPG.Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Items.Database {
    public class RPGItemDatabase {
        static private List<InventoryItem> _items;

        static private bool _isDatabaseLoaded = false;

        static private void ValidateDatabase() {
            if (_items == null) _items = new List<InventoryItem>();
            if (!_isDatabaseLoaded) LoadDatabase();
        }

        static public void LoadDatabase() {
            if (_isDatabaseLoaded) return;
            _isDatabaseLoaded = true;
            LoadDatabaseForce();
        }

        static public void LoadDatabaseForce() {
            ValidateDatabase();
            InventoryItem[] resources = Resources.LoadAll<InventoryItem>(@"Items/Inventory Rework");
            foreach (InventoryItem item in resources) {
                if (!_items.Contains(item)) {
                    _items.Add(item);
                }
            }
        }

        static public void ClearDatabase() {
            _isDatabaseLoaded = false;
            _items.Clear();
        }

        static public InventoryItem GetItem(int id) {
            ValidateDatabase();
            foreach (InventoryItem item in _items) {
                if (item.itemID == id) {
                    return item;
                }
            }
            return null;
        }

        static public InventoryItem GetItem(ItemRarity rarity) {
            ValidateDatabase();
            List<InventoryItem> RandomItemList = new List<InventoryItem>();
            foreach (InventoryItem item in _items) {                
                if (item.itemRarity == rarity) {
                    RandomItemList.Add(item);
                }
            }

            int random = Random.Range(0, RandomItemList.Count);
            if(RandomItemList.Count > 0) {
                return RandomItemList[random];
            }
            return null;
        }
    }
}


