﻿using System;
using UnityEngine;

public class Skill
{
    private string _skillName;
    private Sprite _skillImage;
    private event Action _activateSkill;

    public Skill(string name, Sprite skillImage, Action action)
    {
        _skillName = name;
        _skillImage = skillImage;
        _activateSkill += action;
    }


}
