﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Skills
{
    public class SkillSlotBehavior : MonoBehaviour, IDropHandler
    {
        public void OnDrop(PointerEventData eventData)
        {
            HandleDrop();
            //set image to image of skill
        }

        public void HandleDrop()
        {
          // use script to use skill when pressed hot key
        }
    }
}
