﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasBehavior : MonoBehaviour {

    [SerializeField]private KeyCode _anyKey;
    private void Start() {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    private void LateUpdate() {
        ToggleText();
    }

    private void ToggleText() {
        if (Input.GetKeyDown(_anyKey)) {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        if (Input.GetKeyUp(_anyKey)) {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
