﻿using RPG.Inventory.Hotbar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Abilities {
    [CreateAssetMenu(fileName = "New Spell Ability", menuName ="Ability/Spell")]
    public class Spell : ScriptableObject, IHotbarItem {
        [SerializeField] private string _name = "New Hotbar Item name";
        [SerializeField] private Sprite _icon = null;

        public Sprite icon => _icon;
        public new string name => _name;


        public void Use() {
            Debug.Log("Ability used");
        }       
    }
}
