﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_PatrolPath : MonoBehaviour
{
    const float waypointGizmoRadius = 0.3f;

    private void OnDrawGizmos()
    {
        for(int i=0; i < transform.childCount; i++)
        {
            int j = GetNextIndex(i);
            Gizmos.DrawSphere(GetWayppoint(i), waypointGizmoRadius);
            Gizmos.DrawLine(GetWayppoint(i), GetWayppoint(j));
        }
    }
    
    private static int GetNextIndex(int i)
    {
        return i + 1;

    }

    private Vector3 GetWayppoint(int i)
    {
        return transform.GetChild(i).position;
    }
}
