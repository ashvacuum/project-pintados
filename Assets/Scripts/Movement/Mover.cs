﻿using UnityEngine;
using UnityEngine.AI;
using RPG.Core;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        NavMeshAgent navMeshAgent;
        Animator anim;
        Health health;
        [SerializeField] private float maxSpeed;
        // Start is called before the first frame update
        void Start()
        {
            health = GetComponent<Health>();
            anim = GetComponent<Animator>();
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        // Update is called once per frame
        void Update()
        {
            navMeshAgent.enabled = health.IsAlive;
            UpdateAnimation();
        }

        public void StartMoveAction(Vector3 position, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);            
            MoveTo(position, speedFraction);
        }

        public void MoveTo(Vector3 position, float speed)
        {
            navMeshAgent.destination = position;
            navMeshAgent.isStopped = false;
            navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speed);
        }

        public void Cancel()
        {
            navMeshAgent.isStopped = true;
        }

        void UpdateAnimation()
        {
            Vector3 velocity = navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            anim.SetFloat("forwardSpeed", speed);
        }

    }
}
