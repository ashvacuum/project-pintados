﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventory{
    public interface IItemContainer {
        ItemSlot AddItem(ItemSlot slot);
        void RemoveItem(ItemSlot slot);
        void RemoveAt(int slotIndex);
        void Swap(int index1, int index2);
        bool hasItem(InventoryItem item);
        int GetTotalQuantity(InventoryItem item);
        int GetTotalNumberOfSlots();
    }
}
