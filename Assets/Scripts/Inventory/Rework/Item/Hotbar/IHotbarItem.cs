﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventory.Hotbar {
    public interface IHotbarItem {
        string name { get; }
        Sprite icon { get; }
        void Use();
    }
}
