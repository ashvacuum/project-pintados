﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventory.Hotbar {
    public class Hotbar : MonoBehaviour {

        [SerializeField] private HotbarSlot[] _hotbarSlots = new HotbarSlot[10];

        public void Add(Item itemToAdd) {
            foreach(HotbarSlot hotbarSlot in _hotbarSlots) {
                if (hotbarSlot.AddItem(itemToAdd)) { return; }
            }
        }
    }
}