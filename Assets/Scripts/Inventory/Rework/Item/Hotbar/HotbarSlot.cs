﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Inventory.Hotbar {
    public class HotbarSlot : ItemSlotUI, IDropHandler {
        [SerializeField] protected PlayerInventory _inventory = null;
        [SerializeField] protected TextMeshProUGUI itemQuantityText = null;

        private Item _slotItem = null;

        public override Item slotItem {
            get {
                return _slotItem;
            }
            set {
                _slotItem = value;
                UpdateSlotUI();
            }
        }

        public bool AddItem(Item itemToAdd) {
            if(_slotItem != null) { return false;}

            _slotItem = itemToAdd;
            return true;
        }

        public void UseSlot(int index) {
            if(index != slotIndex) { return; }

            //Use item
        }

        public override void OnDrop(PointerEventData eventData) {
            ItemDragHandler itemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();
            if(itemDragHandler == null) { return; }

            InventorySlot inventorySlot = itemDragHandler.itemSlotUI as InventorySlot;
            if(inventorySlot != null) {
                slotItem = inventorySlot.itemSlot.item;
            }

            HotbarSlot hotbarSlot = itemDragHandler.itemSlotUI as HotbarSlot;
            if(hotbarSlot != null) {
                Item oldItem = slotItem;
                slotItem = hotbarSlot.slotItem;
                hotbarSlot.slotItem = oldItem;
                return;
            }
        }

        public override void UpdateSlotUI() {
            if(slotItem == null) {
                EnableSlotUI(false);
                return;
            }

            _itemIconImage.sprite = slotItem.icon;

            EnableSlotUI(true);

            SetItemQuantityUI();
        }

        private void SetItemQuantityUI() {
            if(slotItem is InventoryItem inventoryItem) {
                if (_inventory.itemContainer.hasItem(inventoryItem)) {
                    int quantityCounts = _inventory.itemContainer.GetTotalQuantity(inventoryItem);
                    itemQuantityText.text = quantityCounts > 1 ? quantityCounts.ToString() : "";
                } else {
                    slotItem = null;
                }
            } else {
                itemQuantityText.enabled = false;
            }
        }

        protected override void EnableSlotUI(bool enable) {
            base.EnableSlotUI(enable);
            itemQuantityText.enabled = enable;
        }
    }
}
