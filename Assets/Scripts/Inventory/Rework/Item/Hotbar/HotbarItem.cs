using UnityEngine;

namespace RPG.Inventory.Hotbar {
    public abstract class Item : ScriptableObject {
        [Header("Basic info")]
        [SerializeField] private string _name = "New Hotbar Item name";
        [SerializeField] private Sprite _icon = null;


        public new string name => _name;
        public abstract string coloredName {get;}
        public Sprite icon => _icon;

        public abstract string GetInfoDisplayText();
    }
}