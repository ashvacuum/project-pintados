﻿using UnityEngine.EventSystems;

namespace RPG.Inventory.Hotbar {
    public class HotbarItemDragHandler : ItemDragHandler {
        public override void OnPointerUp(PointerEventData eventdata) {
            if (eventdata.button == PointerEventData.InputButton.Left) {
                base.OnPointerUp(eventdata);

                if(eventdata.hovered.Count == 0) {
                    (itemSlotUI as HotbarSlot).slotItem = null;
                }
            }
        }
    }
}
