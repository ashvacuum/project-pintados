﻿using RPG.Events;
using RPG.Items.Database;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventory{
    [CreateAssetMenu(fileName = "New Inventory Rework", menuName = "Items/Inventory New")]
    public class PlayerInventory : ScriptableObject{

        [SerializeField] private VoidEvent _OnInventoryItemsUpdated = null;
        [SerializeField] private int itemToAdd;

        public virtual ItemContainer itemContainer { get; } = new ItemContainer(25);

        public void OnEnable() {
            itemContainer.OnItemsUpdated += _OnInventoryItemsUpdated.Raise;
        }

        public void OnDisable() {
            itemContainer.OnItemsUpdated -= _OnInventoryItemsUpdated.Raise;
        }

        [ContextMenu("Test Add")]
        public void TestAdd() {
            
            InventoryItem item = RPGItemDatabase.GetItem(itemToAdd);
            if(item != null) {
                int randomQuant = UnityEngine.Random.Range(0, item.maxStack + 1);
                ItemSlot newSlot = new ItemSlot(item, randomQuant);
                itemContainer.AddItem(newSlot);
            }
        }
    }
}
