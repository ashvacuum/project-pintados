using RPG.Inventory.Hotbar;
using RPG.Items;
using UnityEngine;

namespace RPG.Inventory {
    public abstract class InventoryItem : Item
    {
        [Header("Item data")]
        [SerializeField] private int _itemID = 0;
        [SerializeField] private ItemRarity _itemRarity = ItemRarity.uripon;
        [SerializeField] private Rarity _rarity = null;
        [SerializeField][Min(0)]private int _sellPrice = 1;
        [SerializeField][Min(1)]private int _maxStack = 1;

        public Color rarityColor { get { return _rarity.textColor; } }

        public override string coloredName{
            get {
                string hexColor = ColorUtility.ToHtmlStringRGB(_rarity.textColor);
                return $"<color=#{hexColor}>{name}</color>";
            }
        }
        public int sellPrice => _sellPrice;
        public int maxStack=> _maxStack;

        public Rarity rarity => _rarity;
        public int itemID => _itemID;
        public ItemRarity itemRarity => _itemRarity;
    }
}