﻿using RPG.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Inventory {
    public class InventoryItemDragHandler : ItemDragHandler
    {
        [SerializeField] protected ItemDestroyer _itemDestroyer = null;
        public override void OnPointerUp(PointerEventData eventdata){
            if (eventdata.button == PointerEventData.InputButton.Left) {
                base.OnPointerUp(eventdata);


                foreach (GameObject g in eventdata.hovered) {
                    if(g.GetComponent<Fader>() != null) {
                        InventorySlot thisSlot = itemSlotUI as InventorySlot;
                        _itemDestroyer.Activate(thisSlot.itemSlot, thisSlot.slotIndex);
                        return;
                    }
                }
            }
        }
    }
}
