﻿using RPG.Inventory.Hotbar;
using RPG.Inventory.Weapons;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Inventory{
    public class InventorySlot : ItemSlotUI, IDropHandler
    {
       [SerializeField]protected PlayerInventory _inventory = null;
        [SerializeField] protected WeaponInventory _weapons = null;

        [SerializeField]protected TextMeshProUGUI itemQuantityText = null;

        public override Item slotItem {
            get {
                return itemSlot.item;
            }
            set {

            }
        }
        public ItemSlot itemSlot => _inventory.itemContainer.GetSlotByIndex(slotIndex);

        public override void OnDrop(PointerEventData eventdata) {
            ItemDragHandler itemDragHandler = eventdata.pointerDrag.GetComponent<ItemDragHandler>();

            if (itemDragHandler == null) { return; }
            
            if ((itemDragHandler.itemSlotUI as InventorySlot) != null) {
                _inventory.itemContainer.Swap(itemDragHandler.itemSlotUI.slotIndex, slotIndex);
            }
            WeaponSlot weapon = itemDragHandler.itemSlotUI as WeaponSlot;
            if (weapon != null) {
                _inventory.itemContainer.AddItem(weapon.itemSlot);
                _weapons.weaponContainer.RemoveItem(weapon.itemSlot);
                if((itemSlot.item as EquipmentItem) != null) {
                    _weapons.weaponContainer.EquipItem(itemSlot.item as EquipmentItem);
                }
                Debug.Log("I'm working");
            }

        }

       public override void UpdateSlotUI(){
           if(itemSlot.item == null){
               EnableSlotUI(false);
               return;
           }

           EnableSlotUI(true);

           _itemIconImage.sprite = itemSlot.item.icon;
           itemQuantityText.text = itemSlot.quantity > 1 ? itemSlot.quantity.ToString() : "";
       }

       protected override void EnableSlotUI(bool enable){
           base.EnableSlotUI(enable);
           itemQuantityText.enabled = enable;
       }
    }
}