﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RPG.Events;

namespace RPG.Inventory{
    [RequireComponent(typeof(CanvasGroup))]
    public class ItemDragHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerEnterHandler, IPointerUpHandler, IPointerExitHandler
    {
        [SerializeField] protected ItemSlotUI _itemSlotUI = null;
        [SerializeField] protected HotbarItemEvent OnMouseStartHoverItem = null;
        [SerializeField] protected VoidEvent onMouseEndHoverItem = null;
        private CanvasGroup _canvasGroup = null;

        private Transform _originalParent;
        private bool _isHovering = false;
        public ItemSlotUI itemSlotUI => _itemSlotUI;

        private void Start(){
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnDisable(){
            if(_isHovering){
                onMouseEndHoverItem.Raise();
                _isHovering = false;
            }
        }

        public virtual void OnPointerDown(PointerEventData eventdata){
            if(eventdata.button == PointerEventData.InputButton.Left){
                onMouseEndHoverItem.Raise();
                _originalParent = transform.parent;

                transform.SetParent(transform.parent.parent.parent);

                _canvasGroup.blocksRaycasts = false;
            }
        }

        public virtual void OnDrag(PointerEventData eventdata){
            if(eventdata.button == PointerEventData.InputButton.Left){
                transform.position = Input.mousePosition;
            }
        }

        public virtual void OnPointerUp(PointerEventData eventdata){
            if(eventdata.button == PointerEventData.InputButton.Left){
                transform.SetParent(_originalParent);
                transform.localPosition = Vector3.zero;
                _canvasGroup.blocksRaycasts = true;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_itemSlotUI.slotItem != null) {
                OnMouseStartHoverItem.Raise(_itemSlotUI.slotItem);
                _isHovering = true;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onMouseEndHoverItem.Raise();
            _isHovering = false;
        }
    }
}