﻿
using RPG.Inventory.Hotbar;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RPG.Inventory{
    public abstract class ItemSlotUI : MonoBehaviour, IDropHandler
    {
        [SerializeField] protected Image _itemIconImage = null;
        public int slotIndex{ get; private set;}

        public abstract Item slotItem{ get; set;}

        void OnEnable() => UpdateSlotUI();

        protected virtual void Start(){
            slotIndex = transform.GetSiblingIndex();
            UpdateSlotUI();
        }

        public abstract void OnDrop(PointerEventData eventData);

        public abstract void UpdateSlotUI();

        protected virtual void EnableSlotUI(bool enable){
            _itemIconImage.enabled = enable;
        }
    }
}