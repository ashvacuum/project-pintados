﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RPG.Inventory {
    public class ItemDestroyer : MonoBehaviour {
        [SerializeField]private PlayerInventory _playerInventory;
        [SerializeField]private TextMeshProUGUI areYouSureText = null;

        private int _slotIndex = 0;

        private void OnDisable()=>_slotIndex = -1;

        public void Activate(ItemSlot itemSlot, int slotIndex) {
            _slotIndex = slotIndex;
            areYouSureText.text = $"Are you sure you wish to destroy {itemSlot.quantity}x {itemSlot.item.coloredName}?";

            gameObject.SetActive(true);
        }

        public void DestroyItem() {            
            _playerInventory.itemContainer.RemoveAt(_slotIndex);
            gameObject.SetActive(false);
        }
    }
}
