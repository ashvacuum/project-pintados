﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using RPG.Inventory.Hotbar;

namespace RPG.Inventory{
    [CreateAssetMenu(fileName = "New Consumable Item", menuName = "Items/Consumable")]
    public class ConsumableItem : InventoryItem, IHotbarItem
    {
        [Header("Consumable Data")]
        [SerializeField] private string _useText = "consumable";        

        public override string GetInfoDisplayText()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(rarity.Name).AppendLine();
            builder.Append("<color=green>Use: ").Append(_useText).Append("</color>").AppendLine();
            builder.Append("Max Stack: ").Append(maxStack).AppendLine();
            builder.Append("Sell Price: ").Append(sellPrice).Append(" Gold");

            return builder.ToString();
        }

        public void Use() {
            
        }
    }
}
