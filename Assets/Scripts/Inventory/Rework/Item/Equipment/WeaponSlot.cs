﻿using RPG.Events;
using RPG.Inventory;
using RPG.Inventory.Hotbar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Inventory.Weapons {
    public class WeaponSlot : ItemSlotUI {
        [SerializeField] protected PlayerInventory _inventory = null;
        [SerializeField] private WeaponInventory _weapons = null;
        [SerializeField] private EquipmentType _type;
        [SerializeField] private CharacterStates _states;

        private void Awake() {
            _states = FindObjectOfType<CharacterStates>();
        }

        public EquipmentType type { get { return _type; } }

        private EquipmentItem _equipmentItem = null;

        public override Item slotItem { get => _equipmentItem; set { } }

        public ItemSlot itemSlot => _weapons.weaponContainer.GetSlotByIndex(slotIndex);

        public override void OnDrop(PointerEventData eventData) {
            ItemDragHandler itemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();

            if (itemDragHandler == null) { return; }
            InventorySlot slotToCheck = itemDragHandler.itemSlotUI as InventorySlot;
            if (slotToCheck != null) {
                EquipmentItem item = itemDragHandler.itemSlotUI.slotItem as EquipmentItem;
                if (item != null) {
                    if (item.type.Equals(_type)) {
                        if (_weapons.weaponContainer.EquipItem(item, slotIndex)) {
                            _inventory.itemContainer.RemoveItem(slotToCheck.itemSlot);
                            return;
                        }
                    }
                } else {
                    if (slotItem != null) {
                        if (item.type.Equals(_type)) {
                            if (_weapons.weaponContainer.EquipItem(item, slotIndex)) {

                            }
                            _inventory.itemContainer.AddItem(itemSlot);
                        }
                    }
                }
            }
        }

        public override void UpdateSlotUI() {
            if (itemSlot.item == null) {
                EnableSlotUI(false);
                if(_type == EquipmentType.Weapon){
                    _states.WeaponEquip(WeaponTypes.BareHanded);
                }
                return;
            }

            EnableSlotUI(true);

            _itemIconImage.sprite = itemSlot.item.icon;
            EquipmentItem newItem = itemSlot.item as EquipmentItem;
            if (newItem != null) {
                if(_type == EquipmentType.Weapon) {                                      
                    _states.WeaponEquip(newItem.weaponClass);                    
                }
            } 


        }

        
    }
}
