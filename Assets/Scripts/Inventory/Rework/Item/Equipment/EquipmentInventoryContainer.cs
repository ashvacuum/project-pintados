﻿using RPG.Inventory;
using RPG.Inventory.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EquipmentInventoryContainer : MonoBehaviour, IItemContainer {
    private ItemSlot[] _itemSlots = new ItemSlot[0];

    public Action OnChangeEquipment;

    public EquipmentInventoryContainer(int size) => _itemSlots = new ItemSlot[size];

    public ItemSlot GetSlotByIndex(int index) => _itemSlots[index];

    public bool EquipItem(EquipmentItem item, int index) {
        if (item == null) {
            Debug.Log("cant detect anything");
            return false;
        }
        _itemSlots[index] = new ItemSlot(item);
        OnChangeEquipment.Invoke();
        return true;
    }

    public bool EquipItem(EquipmentItem item) {
        for (int i = 0; i < _itemSlots.Length; i++) {
            EquipmentItem slot = _itemSlots[i].item as EquipmentItem;
            if (slot != null) {
                if (slot.type == item.type) {
                    _itemSlots[i] = new ItemSlot(item);
                    OnChangeEquipment.Invoke();
                    return true;
                }
            }
        }
        return false;
    }

    public ItemSlot AddItem(ItemSlot slot) {
        for (int i = 0; i < _itemSlots.Length; i++) {
            if (_itemSlots[i].item != null) {
                if (_itemSlots[i].item == slot.item) {
                    int slotRemaining = _itemSlots[i].item.maxStack - _itemSlots[i].quantity;

                    if (slot.quantity <= slotRemaining) {
                        _itemSlots[i].quantity += slot.quantity;

                        slot.quantity = 0;

                        OnChangeEquipment.Invoke();

                        return slot;
                    } else if (slotRemaining > 0) {
                        _itemSlots[i].quantity += slotRemaining;

                        slot.quantity -= slotRemaining;
                    }
                }
            }
        }

        for (int i = 0; i < _itemSlots.Length; i++) {
            if (_itemSlots[i].item == null) {
                if (slot.quantity <= slot.item.maxStack) {
                    _itemSlots[i] = slot;

                    slot.quantity = 0;

                    OnChangeEquipment.Invoke();

                    return slot;
                } else {
                    _itemSlots[i] = new ItemSlot(slot.item, slot.item.maxStack);

                    slot.quantity -= slot.item.maxStack;
                }
            }
        }

        OnChangeEquipment.Invoke();

        return slot;
    }

    public int GetTotalQuantity(InventoryItem item) {
        int totalCount = 0;

        foreach (ItemSlot slot in _itemSlots) {
            if (slot.item == null) { continue; }
            if (slot.item != item) { continue; }
            totalCount += slot.quantity;
        }
        return totalCount;
    }

    public bool hasItem(InventoryItem item) {
        foreach (ItemSlot slot in _itemSlots) {
            if (slot.item == null) { continue; }
            if (slot.item != item) { continue; }

            return true;
        }
        return false;
    }

    public void RemoveAt(int slotIndex) {
        if (slotIndex < 0 || slotIndex > _itemSlots.Length - 1) {
            return;
        }
        _itemSlots[slotIndex] = new ItemSlot();
        OnChangeEquipment.Invoke();
    }

    public void RemoveItem(ItemSlot slot) {
        for (int i = 0; i < _itemSlots.Length; i++) {
            if (_itemSlots[i].item != null) {
                if (_itemSlots[i].item == slot.item) {
                    if (_itemSlots[i].quantity < slot.quantity) {
                        slot.quantity -= _itemSlots[i].quantity;

                        _itemSlots[i] = new ItemSlot();

                        OnChangeEquipment.Invoke();
                    } else {
                        _itemSlots[i].quantity -= slot.quantity;

                        if (_itemSlots[i].quantity == 0) {
                            _itemSlots[i] = new ItemSlot();
                            OnChangeEquipment.Invoke();

                            return;
                        }
                    }
                }
            }
        }
    }

    public virtual void Swap(int index1, int index2) {
        ItemSlot first = _itemSlots[index1];
        ItemSlot second = _itemSlots[index2];

        if (first == second) { return; }

        if (second != null) {
            if (first.item == second.item) {
                int slotRemaining2 = second.item.maxStack - second.quantity;

                if (first.quantity <= slotRemaining2) {
                    second.quantity += first.quantity;

                    _itemSlots[index1] = new ItemSlot();

                    OnChangeEquipment.Invoke();

                    return;
                }
            }
        }

        _itemSlots[index1] = second;
        _itemSlots[index2] = first;

        OnChangeEquipment.Invoke();

    }

    public int GetTotalNumberOfSlots() {
        return _itemSlots.Length;
    }
}
