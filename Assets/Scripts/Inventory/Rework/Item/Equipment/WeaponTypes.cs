﻿namespace RPG.Inventory.Weapons {
    public enum WeaponTypes {
        Sword = 0,
        TwoHandSword = 1,
        Axe = 2,
        BareHanded = 3,
        NotAWeapon = 4
    }
}
