﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using RPG.Inventory;

namespace RPG.Inventory.Weapons {
    [CreateAssetMenu(fileName = "New Weapon Item", menuName = "Items/Weapons")]
    public class EquipmentItem : InventoryItem
    {
        private const string Value = "</color>";
        [Header("Weapon Data")]
        [SerializeField] protected EquipmentType _type;
        [SerializeField] protected WeaponTypes _weaponClass;
        [SerializeField] protected string _useText = "consumable";
        [SerializeField] protected GameObject _meshPrefab;
        [SerializeField] protected int _damageArmor = 0;

        public int damageArmor {  get { return _damageArmor; } }
        public WeaponTypes weaponClass {  get { return _weaponClass; } }
        public EquipmentType type { get { return _type; } }

        

        public override string GetInfoDisplayText()
        {
            string swap = _type.Equals(EquipmentType.Weapon) ? ">Damage: " : ">Armor: ";
            StringBuilder builder = new StringBuilder();
            builder.Append(_type.ToString()).AppendLine();
            builder.Append("<color=green>Use: ").Append(_useText).Append(Value).AppendLine();
            builder.Append("<color=#").Append(ColorUtility.ToHtmlStringRGB(rarity.textColor)).Append(swap).Append(_damageArmor.ToString()).Append(Value).AppendLine();            
            builder.Append("Sell Price: ").Append(sellPrice).Append(" Gold");
            return builder.ToString();
        }
    }

    public enum EquipmentType {
        Weapon,
        ChestArmor,
        LegArmor,
        HeadArmor
    }
}
