﻿using RPG.Events;
using UnityEngine;

namespace RPG.Inventory.Weapons {
    [CreateAssetMenu(fileName = "New Weapon Inventory", menuName = "Items/Weapon Inventory")]
    public class WeaponInventory : ScriptableObject {
        [SerializeField] private VoidEvent _OnEquipmentUpdated = null;       

        public ItemContainer weaponContainer { get; } = new ItemContainer(3);

        public void OnEnable() {
            weaponContainer.OnItemsUpdated += _OnEquipmentUpdated.Raise;
        }

        public void OnDisable() {
            weaponContainer.OnItemsUpdated -= _OnEquipmentUpdated.Raise;
        }

        public int ReturnDamage() {
            int damage = 0;
            for(int i = 0; i < weaponContainer.GetTotalNumberOfSlots(); i++) {
                EquipmentItem equipment = weaponContainer.GetSlotByIndex(i).item as EquipmentItem;
                if(equipment != null) {
                    if(equipment.type == EquipmentType.Weapon) {
                        damage += equipment.damageArmor;
                    }
                }
            }
            return damage;
        }

        public int ReturnArmor() {
            int armor = 0;
            for (int i = 0; i < weaponContainer.GetTotalNumberOfSlots(); i++) {
                EquipmentItem equipment = weaponContainer.GetSlotByIndex(i).item as EquipmentItem;
                if (equipment != null) {
                    if (equipment.type != EquipmentType.Weapon) {
                        armor += equipment.damageArmor;
                    }
                }
            }
            return armor;
        }
    }

}
