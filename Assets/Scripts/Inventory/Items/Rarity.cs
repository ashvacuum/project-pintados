﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Items {
    [CreateAssetMenu(fileName = "New Rarity", menuName = "Items/Rarity")]
    public class Rarity : ScriptableObject {
        [SerializeField]private new string name = "New Rarity Name";
        [SerializeField] private Color _textColor = new Color(1f, 1f, 1f, 1f);

        public string Name => name;
        public Color textColor => _textColor;
    }
}
