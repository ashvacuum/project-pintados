﻿using RPG.Inventory;
using RPG.Items;
using RPG.Items.Database;
using UnityEngine;

namespace RPG.Loot {
    [System.Serializable]
    public struct LootDetails {
        public ItemRarity weaponRarity;
        public int dropWeight;
    }

    public class LootDrop : MonoBehaviour {
        [SerializeField] private PlayerInventory _inventory = null;
        public PlayerInventory inventory => _inventory;

        public LootDetails[] lootController;

        public InventoryItem GiveLoot() {
            int minDrop = 0;
            int maxDrop = 0;
            int sumDrop = 0;
            foreach(LootDetails loot in lootController) {
                sumDrop += loot.dropWeight;
            }

            int randomizer = Random.Range(0, sumDrop);

            foreach (LootDetails loot in lootController) {
                maxDrop += loot.dropWeight;
                if(randomizer >= minDrop && randomizer < maxDrop) {
                    return RPGItemDatabase.GetItem(loot.weaponRarity);                    
                }
                minDrop = maxDrop;
            }
            return null;
        }
    }
}
