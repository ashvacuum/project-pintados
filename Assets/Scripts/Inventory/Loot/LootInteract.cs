﻿using RPG.Interaction;
using RPG.Inventory;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

namespace RPG.Loot {
    public class LootInteract : MonoBehaviour, IInteractable {
        [SerializeField]private TextMeshProUGUI text;

        private ItemSlot _slot;

        LootDrop _lootDrop;

        private void Awake() {
            _lootDrop = FindObjectOfType<LootDrop>();
        }

        public void SetUp(InventoryItem item, int quantity) {
            _slot.item = item;
            _slot.quantity = quantity;
            text.text = _slot.item.name;
            text.color = _slot.item.rarityColor;
        }

        public void SetUp(InventoryItem item) {
            _slot.item = item;
            _slot.quantity = UnityEngine.Random.Range(0, item.maxStack + 1);
            text.text = _slot.item.name;
            text.color = _slot.item.rarityColor;
        }



        public void Interact() {
            _lootDrop.inventory.itemContainer.AddItem(_slot);
            Destroy(this.gameObject);
        }
    }
}
