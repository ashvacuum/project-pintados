﻿using RPG.Inventory;
using RPG.Items.Database;
using UnityEngine;

namespace RPG.Loot {
    public class LootContainer : MonoBehaviour {

        [SerializeField]private int numberToDrop = 0;
        [SerializeField]private GameObject thingToDrop = null;

        private LootDrop _lootDrop;

        private void Awake() {
            _lootDrop = FindObjectOfType<LootDrop>();
        }

        public void DropItems() {            
            for(int i = 0; i < numberToDrop; i++) {
                GameObject g = Instantiate(thingToDrop as GameObject, SpawnAtRandomLocation(transform.position, 1.5f), Quaternion.identity);                
                g.GetComponent<LootInteract>().SetUp(_lootDrop.GiveLoot());
            }
        }

        private bool AddItem(ItemSlot slot) {
            if (_lootDrop.inventory.itemContainer.AddItem(slot) != null) {
                return true;
            }
            return false;
        }

        public Vector3 SpawnAtRandomLocation(Vector3 position, float range) {
            float randX = Random.Range(position.x - range, position.x + range);
            float randZ = Random.Range(position.z - range, position.z + range);
            Vector3 newSpawn = new Vector3(randX, position.y, randZ);
            return newSpawn;
        }

    }
}
