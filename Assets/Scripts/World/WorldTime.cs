﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldTime : MonoBehaviour
{

    private float mins;
    [SerializeField] private int hours;
    [SerializeField] private string meridiem;
    [SerializeField] private int minutes;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        WorldClock();
        //convert float mins to Int
        minutes = Mathf.FloorToInt(mins);

    }

    void WorldClock()
    {
        //Mins
        mins += 5 * Time.deltaTime;
        
        if (mins > 60)
        {
            mins = 0;
            hours += 1;
        }

        //Hour
        if (hours == 24 && mins > 0)
        {
            hours = 00;
        }
    }
}
