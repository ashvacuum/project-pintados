﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementHumanoid : MonoBehaviour
{
    //scripts//
    CharacterController controller;
    CharacterStats ChStats;
    Animator charAnim;
    CharacterStates chStates;
    //float//
    private float forwardSmoothing = 0.0f;
    private float moveHor, moveVer;
    private float rotationSpeed = 450;
    [SerializeField] private float MoveSpeed = 0.8f;
    [SerializeField] private float idleTime = 0.0f;
    private float _revertForward;
    private float _verticalVelocity;
    [SerializeField] private float _gravity = 14.0f;
    [SerializeField] private float _jumpForce = 100.0f;
    //Transforms//
    private Quaternion targetRotation;
    //bools//
    private bool isRunning;
    private bool _isJumping;
    [SerializeField] private bool isGrounded = false;

    ////////////////////\\\\\\\\\\\\\\\\\\\\
    //For Animator Variables\\
    [System.Serializable]
    public class AnimationSettings
    {
        public string forwardFloat = "Forward";
        public string groundedBool = "isGrounded";
        public string jumpBool = "isJumping";
    }
    [SerializeField]
    public AnimationSettings animations;
    //For Preference Input Names\\
    [System.Serializable]
    public class InputSettings
    {
        public string verticalAxis = "Vertical";
        public string horizontalAxis = "Horizontal";
        public string jumpButton = "Jump";
        public string sprintButton = "Sprint";
        public string reloadButton = "Reload";
        public string dropWeaponButton = "DropWeapon";
        public string sheatheWeaponButton = "Sheathe";
    }
    [SerializeField]
    InputSettings input;
    ////////////////////\\\\\\\\\\\\\\\\\\\\


    void Start()
    {
        chStates = GetComponent<CharacterStates>();
        ChStats = new CharacterStats();
        charAnim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
    }
    void Update()
    {
        CharacterInputMovement();
        
        CharacterGravity();
        
    }

    void CharacterInputMovement()
    {
        //this method is made just to detect if the player is moving and set state to moving

        moveVer = Input.GetAxisRaw(input.verticalAxis);
        moveHor = Input.GetAxisRaw(input.horizontalAxis);
        isRunning = Input.GetButton(input.sprintButton);
        _isJumping = Input.GetButton(input.jumpButton);
        float targetForward = 0.5f;
        _revertForward = 0f;
        //for sprinting
        if (isRunning) { targetForward = 1f; }

        //call the moving method to move character
        if (moveHor != 0 || moveVer != 0)
        {
            chStates.plyrStates = CharacterStates.PlayerStates.Moving;
            //for Rotation
            CharacterTurningAndMovement();
            //blend Tree forward smoothing
            forwardSmoothing = Mathf.MoveTowards(forwardSmoothing, targetForward, 1.5f * Time.deltaTime);
        }
        else if (moveVer == 0 && moveHor == 0)
        {
            forwardSmoothing = Mathf.MoveTowards(forwardSmoothing, _revertForward, 1f * Time.deltaTime);
            //revert the state to IDLE whenever the horizontal and vertical is 0
            chStates.plyrStates = CharacterStates.PlayerStates.Idle;
        }
        //calling function for animation
        Animate(forwardSmoothing);
    }
    //
   
    //for moving character without the need of animation, also turning
    
    void CharacterTurningAndMovement()
    {
        //ask if it is moving
        if (chStates.plyrStates == CharacterStates.PlayerStates.Moving)
        {
            //!!!!!!!!!!!!!!!!need some help to shorten this if its not really a good code.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!???!?!?!?!?///////////////////
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsTag("Movements"))
            {
                Vector3 input = Vector3.zero; //new Vector3(moveHor, 0, moveVer);
                input.x = moveHor;
                input.z = moveVer;
                if (input != Vector3.zero)
                {
                    targetRotation = Quaternion.LookRotation(input);
                    transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);
                }
                Vector3 motion = input;
                controller.Move(motion * MoveSpeed * Time.deltaTime);
            }
        }
    }

    void CharacterGravity()
    {
        if (controller.isGrounded)
        {
            _verticalVelocity = -_gravity * Time.deltaTime;
            //if jump is pressed
            CharacterJumpRoll();
            //MoveSpeed = 0.8f;
            charAnim.SetBool("Jumping", false);
        }
        else
        {
            _verticalVelocity -= _gravity * Time.deltaTime;
            charAnim.SetBool("Jumping", true);

        }
        Vector3 moveVector = Vector3.zero;
        moveVector.y = _verticalVelocity;
        controller.Move(moveVector * Time.deltaTime);

        //for jump and roll
        void CharacterJumpRoll()
        {
            if (_isJumping)
            {
                if (chStates.plyrStates == CharacterStates.PlayerStates.Moving || chStates.plyrStates == CharacterStates.PlayerStates.Idle)
                {
                    if (chStates.plyrWeapons == CharacterStates.PlayerWeapons.Bare)
                    {
                        //controller.height = charAnim.GetFloat("ColliderHeight");

                        //jump
                        _verticalVelocity = _jumpForce;
                        
                        charAnim.SetTrigger("Jump");
                    }
                    else if (chStates.plyrWeapons != CharacterStates.PlayerWeapons.Bare)
                    {
                        //roll
                        charAnim.SetTrigger("roll");
                    }
                }
            }
            else if (!_isJumping)
            {
                if (charAnim.GetCurrentAnimatorStateInfo(0).IsName("Unarmed-Land"))
                {
                    //MoveSpeed = 3;
                }
            }

        }
    }
    //for animations
    public void Animate(float forward)
    {
        charAnim.SetFloat("Forward", forward);
    }
    


    /*
        void RandomIdles()
        {
            if (chStates.plyrStates == CharacterStates.PlayerStates.Idle)
            {
                idleTime += Time.deltaTime;
            }else if (chStates.plyrStates != CharacterStates.PlayerStates.Idle)
            {
                idleTime = 0f;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Ground"))
            {
                isGrounded = true;

                Debug.Log("Grounded");
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Ground"))
            {
                isGrounded = false;

                Debug.Log("Not Grounded");
            }
        }
        */
}
