﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatCombos : MonoBehaviour
{
    //SCRIPTS\\
    CharacterStates chStates;
    CharacterMovementHumanoid chMovement;
    Animator charAnim;
    //BOOL\\
    private bool _lightAttack = false;
    private bool _lightAttackHold = false;
    //private bool _lightAttackDouble = false;

    private bool _heavyAttack = false;
    private bool _heavyAttackHold = false;
    //private bool _heavyAttackDouble = false;
    [SerializeField] private bool _HeavyHold = false;

    private bool _fromIdle = false;
    [SerializeField] private bool _sheathe;
    [SerializeField] private bool _leftMouseUp = false;
    [SerializeField] private bool _rightMouseUp = false;
    //FLOAT\\
    [SerializeField] private float comboReset;
    [SerializeField] private float comboAttackReset;
    [SerializeField] private float _startTime0, _endTime0;
    [SerializeField] private float _startTime1, _endTime1;
    //INT\\
    [SerializeField] private int comboCounter = 0;

    
    
    //CUSTOM CLASS\\
    [System.Serializable]
    public class AttackAnimationNames
    {
        public string LightAttack1 = "AtkL3Tail";
        public string LightAttack2 = "AtkR2Tail";
        public string LightAttack3 = "AtkR1Tail";
        public string HeavyAttack1 = "AtkR3Tail";
        public string HeavyAttack2 = "AtkL1Tail";
        public string HeavyAttack4 = "AtkL2Tail";
    }
    [SerializeField]
    AttackAnimationNames attackAnim;

    [System.Serializable]
    public class InputSettings
    {
        public string dropWeaponButton = "DropWeapon";
        public string sheatheWeaponButton = "Sheathe";
    }
    [SerializeField]
    InputSettings input;

    //START OF CODE\\
    void Awake()
    {
        charAnim = GetComponent<Animator>();
        chStates = GetComponent<CharacterStates>();
        chMovement = GetComponent<CharacterMovementHumanoid>();
        _sheathe = true;
        //Initialization
        _startTime0 = 0f;
        _endTime0 = 0f;
        _startTime1 = 0f;
        _endTime1 = 0f;

    }

    void Update()
    {
        //Use to sheathe and Unsheathe weapons
        //UnsheatheWeapon();
        //Input for Mouse Attacks
        LeftClickSmash();
        RightClickSmash();
        //other method for checking
        CountDownCombo();
        IdleChecker();
        ////////////////////////////////////////////////


        //checks the state of weapon before extracting combos
        if (chStates.plyrWeapons == CharacterStates.PlayerWeapons.Bare) { BareCombo(); }
        else if (chStates.plyrWeapons == CharacterStates.PlayerWeapons.SwordandShield) { SwordandShieldCombo(); }
        else if (chStates.plyrWeapons == CharacterStates.PlayerWeapons.Axe) { AxeCombo();  }
        else if (chStates.plyrWeapons == CharacterStates.PlayerWeapons.TwoHandSword) { TwoHandCombo(); }
        /*
        if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack4) && _heavyAttack)
        {
           
            Debug.Log("AtkL2TailTest");
            charAnim.SetInteger("attackCombo", 204);

        }*/
    }

    public void UnsheatheWeapon()
    {
        if (chStates == null) return;
        if (chStates.plyrWeapons != CharacterStates.PlayerWeapons.Bare)
        {
                //if (_sheathe)
               // {
                charAnim.SetTrigger("armWeapon");
                _sheathe = false;
                    
                //} 
                /* 
                else if (!_sheathe)
                {
                charAnim.SetTrigger("disArmWeapon");
                _sheathe = true;
                }
            
            */
        }
    }
    void LeftClickSmash()
    {

        /////////\\\\\\\\\\
        if (Input.GetMouseButtonDown(0))
        {
            _startTime0 = Time.time;
            _leftMouseUp = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _endTime0 = Time.time;
            _leftMouseUp = true;
        }
        //Long Click
        if (_leftMouseUp)
        { 
            if (_endTime0 - _startTime0 > 0.5f)
            {
                //Debug.Log("Long Left Click");
                _lightAttackHold = true;

                _startTime0 = 0f;
                _endTime0 = 0f;
            }
            else if (_endTime0 - _startTime0 < 0.5f && _endTime0 - _startTime0 != 0)
            {
                chStates.plyrStates = CharacterStates.PlayerStates.Attack;
                _lightAttack = true;
                comboAttackReset = comboReset;
                //Debug.Log("Single Left Click");
                _startTime0 = 0f;
                _endTime0 = 0f;
            }
        }
    }
    void RightClickSmash()
    {
        
        if (Input.GetMouseButtonDown(1))
        {
            _startTime1 = Time.time;
            _rightMouseUp = false;
        }
        if (Input.GetMouseButtonUp(1))
        {
            _endTime1 = Time.time;
            _rightMouseUp = true;
            _HeavyHold = false;
        }
        //Long Click
        /*
        
        */
 
        if (_rightMouseUp)
        {
            if (_endTime1 - _startTime1 > 0.5f)
            {
                Debug.Log("Long Right Click");
                _heavyAttackHold = true;
                _startTime1 = 0f;
                _endTime1 = 0f;
            }//change to something that return true when hold.
            else if (_endTime1 - _startTime1 < 0.5f && _endTime1 - _startTime1 != 0)
            {
                chStates.plyrStates = CharacterStates.PlayerStates.Attack;
                _heavyAttack = true;
                comboAttackReset = comboReset;
                Debug.Log("Single Right Click");
                _startTime1 = 0f;
                _endTime1 = 0f;
            }
        }
    }

    void CountDownCombo()
    {
        if (comboAttackReset >= 0)
        {
            comboAttackReset -= Time.deltaTime;
        }
        else if (comboAttackReset <= 0)
        {
            ResetCombo();
        }
    }
    void ResetCombo()
    {
         _lightAttack = false;
         _lightAttackHold = false;
        // _lightAttackDouble = false;

         _heavyAttack = false;
         _heavyAttackHold = false;
        // _heavyAttackDouble = false;

        charAnim.SetInteger("attackCombo", 0);
        charAnim.SetBool("returnToMovement", true);
    }
    void IdleChecker()
    {
        if (charAnim.GetCurrentAnimatorStateInfo(0).IsTag("Movements"))
        {
            
            _fromIdle = true;
            charAnim.SetBool("returnToMovement", false);
            //charAnim.SetBool("returnToNonCombatMovement", false);
        }
        else if (!charAnim.GetCurrentAnimatorStateInfo(0).IsTag("Movements"))
        {
            _fromIdle = false;
        }/*
        else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName("Sword Movement"))
        {
            _fromIdle = true;
            charAnim.SetBool("returnToCombatMovement", false);
            Debug.Log("GAGO SWORD");
        }
        else if (!charAnim.GetCurrentAnimatorStateInfo(0).IsName("Sword Movement"))
        {
            _fromIdle = false;
        }*/
        //Check if Moving, if true, reset combos
        if (chStates.plyrStates == CharacterStates.PlayerStates.Moving)
        {
            ResetCombo();
        }
    }
    //shortcut method to set LightAttack and HeavyAttack to false;
    void lightHeavyButtonDisabler()
    {
        _lightAttack = false;
        _heavyAttack = false;
        _lightAttackHold = false;
        _heavyAttackHold = false;
    }

    //Combo 
    void LightHeavyCombo()
    {
        //Light Attack Bare Combo
        if (_lightAttack)
        {

            //combo 1
            if (_fromIdle)
            {
                //attack 1
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 101);
            }

            //Light Attack Combo
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack1) && _lightAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 102);
            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack2) && _lightAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 103);
            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack3) && _lightAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 104);
            }
            //Heavy Attack Transition
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack1) || charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack4) && _lightAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 101);
            }  
        }

        if(chStates.plyrWeapons == CharacterStates.PlayerWeapons.SwordandShield) { return; }
        //Heavy Attack Bare Combo
        if (_heavyAttack)
        {
            if (_fromIdle)
            {
                //attack 1
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 201);
            }
            //Heavy Attack Combo
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack1) && _heavyAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 202);
            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack2) && _heavyAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 203);
            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack4) && _heavyAttack)
            {
                lightHeavyButtonDisabler();
                Debug.Log("Im Not Not Workign");
                charAnim.SetInteger("attackCombo", 204);
            }
            //Light Attack Transition
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack1) || charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack3) && _heavyAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 201);
            }
        }
    }
    //combos per Weapon
    void BareCombo()
    {
        LightHeavyCombo();
        //specials
        
        if (_lightAttack)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack2) && _lightAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 302);
            }
        }
        if (_heavyAttack)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack2) && _heavyAttack)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 301);
            }
        }
        
    }
    void SwordandShieldCombo()
    {
        LightHeavyCombo();



        if (_lightAttackHold)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack1) && _lightAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 301);

            }else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack2) && _lightAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 302);
            }
        }
    }
    void TwoHandCombo()
    {
        LightHeavyCombo();
        if (_lightAttackHold)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack2) && _lightAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 301);

            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack1) && _lightAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 302);
            }
        }
        if (_heavyAttackHold)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack1) && _heavyAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 302  );
            }
            else if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack4) && _heavyAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 303);
            }
        }
    }
    void AxeCombo()
    {
        LightHeavyCombo();
        if (_lightAttackHold)
        {
            if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.LightAttack2) && _lightAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 301);

            }
        }
        if (_heavyAttackHold)
        {
                if (charAnim.GetCurrentAnimatorStateInfo(0).IsName(attackAnim.HeavyAttack1) && _heavyAttackHold)
            {
                lightHeavyButtonDisabler();
                charAnim.SetInteger("attackCombo", 302);
            }
        }

    }
}
