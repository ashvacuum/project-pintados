﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSystem : MonoBehaviour
{

    private Animator anim;
    [SerializeField] private float Damage;
    [SerializeField] private float AttackSpeed; 
    CharacterStates ChStates;
    CharacterStats ChStats;
    [SerializeField] private int rr;
    [SerializeField] private int comboRange;

    private void Start()
    {
        anim = GetComponent<Animator>();
        ChStates = GetComponent<CharacterStates>();

        ChStats = new CharacterStats();

        
    }
    private void Update()
    {
        
        Attack();
        comboRange = ChStats.RandomCombo;
        //can only defend if the Sword and shield is equiped
        if (ChStates.plyrWeapons == CharacterStates.PlayerWeapons.SwordandShield)
        {
            Defend();
        }
       

    }

    public void SetDamage()
    {
        ChStats.PlayerDamagePerSecond = Damage;
    }

    public void Attack()
    {
        if(ChStates.plyrStates == CharacterStates.PlayerStates.Idle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rr = (Random.Range(0, 5));
                ChStats.RandomCombo = rr;

                ChStates.plyrStates = CharacterStates.PlayerStates.Attack;
                if (ChStates.plyrStates == CharacterStates.PlayerStates.Attack)
                {
                    if (comboRange >= 0 && rr < 2)
                    {
                        Atk1();
                    }
                    if (comboRange >= 2 && rr < 4)
                    {
                        Atk2();
                    }
                    else
                    {
                        Atk3();
                    }
                }

            }
        }



    }
    public void doDamage() {
        //do/perform damage get damage from class | call by using animation event;
    }
    public void Atk1() {
        //set damage from class
        ChStates.plyrStates = CharacterStates.PlayerStates.Attack;
        anim.SetInteger("RandomAttacks", 1);
    }
    public void Atk2() {
        //set damage from class
        ChStates.plyrStates = CharacterStates.PlayerStates.Attack;
        anim.SetInteger("RandomAttacks", 2);
    }
    public void Atk3() { }
    
    void Defend()
    {
        //if shield equiped
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("toBlock");
            ChStates.plyrStates = CharacterStates.PlayerStates.Defend;
            
        }
        if(ChStates.plyrStates == CharacterStates.PlayerStates.Defend)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                anim.SetBool("isBlocking", true);
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                anim.SetBool("isBlocking", false);
                ChStates.plyrStates = CharacterStates.PlayerStates.Idle;
            }
        }

    }

   
}
