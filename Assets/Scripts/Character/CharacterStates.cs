﻿using RPG.Inventory.Weapons;
using System.Collections;
using System.Collections.Generic;
using RPG.Combat;
using UnityEngine;

public class CharacterStates : MonoBehaviour
{
    CharacterStats ChStats;
    [SerializeField]Animator anim;

    [SerializeField]private CombatCombos _combos;

    private WeaponPrefabSwitcher _weaponPrefabSwitcher;

    public enum PlayerStates{
        Idle,
        Defend,
        Moving,
        Jump,
        Attack,
        Staggered,
        Death
    } public PlayerStates plyrStates;

    public enum PlayerWeapons{
        Bare,
        SwordandShield,
        Axe,
        TwoHandSword
        

    } public PlayerWeapons plyrWeapons;
         



    void Awake()
    {
        _weaponPrefabSwitcher = GetComponent<WeaponPrefabSwitcher>();
        _combos = GetComponent<CombatCombos>();
        anim = GetComponent<Animator>();
        plyrStates = PlayerStates.Idle;
        plyrWeapons = PlayerWeapons.Bare;
        ChStats = new CharacterStats();
    }
    
    void Update()
    {
        //idk if needed
        switch (plyrStates){
            case PlayerStates.Idle:
                
                break;  
            case PlayerStates.Attack:
                //do damage
                
                break;
                
        }switch (plyrWeapons){
            case PlayerWeapons.Bare:
                Bare_weapon();
                break;
            case PlayerWeapons.SwordandShield:
                break;
            case PlayerWeapons.Axe:
                //MousetoLookMovement = true
                break;
            case PlayerWeapons.TwoHandSword:
                //MousetoLookMovement = true
                break;
        }
        //methods

        //WeaponEquip();
    }



    public void WeaponEquip(WeaponTypes types)
    {

        //PlayerStates.Attack   this sets the current state to attack
        //set animations for attack

        //--if Should be replace by CurrentWeapon Bool not key press--// e.g if(EquipSword)

        switch (types) {
            case WeaponTypes.BareHanded:
                anim.SetTrigger("armBare");
                //setting the the Other Weapon to false
                anim.SetBool("isSword", false);
                anim.SetBool("isAxe", false);
                anim.SetBool("is2HandSword", false);
                plyrWeapons = PlayerWeapons.Bare;
                _combos.UnsheatheWeapon();
                _weaponPrefabSwitcher.WeaponSwitch(WeaponTypes.BareHanded);
                break;

            case WeaponTypes.Axe:
                Debug.Log("Weapon switched to axe");
                anim.SetBool("isAxe", true);
                //setting the  Other Weapon to false
                anim.SetBool("isSword", false);
                anim.SetBool("is2HandSword", false);
                plyrWeapons = PlayerWeapons.Axe;
                _combos.UnsheatheWeapon();
                _weaponPrefabSwitcher.WeaponSwitch(WeaponTypes.Axe);
                break;

            case WeaponTypes.Sword:
                anim.SetBool("isSword", true);
                //setting the  Other Weapon to false
                anim.SetBool("isAxe", false);
                anim.SetBool("is2HandSword", false);
                plyrWeapons = PlayerWeapons.SwordandShield;
                _combos.UnsheatheWeapon();
                Debug.Log(WeaponTypes.Sword);
                _weaponPrefabSwitcher.WeaponSwitch(WeaponTypes.Sword);
                break;

            case WeaponTypes.TwoHandSword:
                anim.SetBool("is2HandSword", true);
                //setting the Other Weapon to false
                anim.SetBool("isSword", false);
                anim.SetBool("isAxe", false);
                plyrWeapons = PlayerWeapons.TwoHandSword;
                _combos.UnsheatheWeapon();
                Debug.Log(WeaponTypes.TwoHandSword);
                _weaponPrefabSwitcher.WeaponSwitch(WeaponTypes.TwoHandSword);
                break;                    
        }
        /*

        if (types == WeaponTypes.BareHanded)
        {        
            anim.SetTrigger("armBare");
            //setting the the Other Weapon to false
            anim.SetBool("isSword", false);
            anim.SetBool("isAxe", false);
            anim.SetBool("is2HandSword", false);
            plyrWeapons = CharacterStates.PlayerWeapons.Bare;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {       
            anim.SetBool("isSword", true);
            //setting the  Other Weapon to false
            anim.SetBool("isAxe", false);
            anim.SetBool("is2HandSword", false);
            plyrWeapons = CharacterStates.PlayerWeapons.SwordandShield;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            anim.SetBool("is2HandSword", true);
            //setting the Other Weapon to false
            anim.SetBool("isSword", false);
            anim.SetBool("isAxe", false);
            plyrWeapons = CharacterStates.PlayerWeapons.TwoHandSword;
        }
        */

    } 

    void Bare_weapon() {
        //hides the weapon
        //attack speed set to
    }
    void SwordAndShield_weapon() {
        //sword appear
        //attack speed set to
    }
    void Axe_weapon() { }
    void TwoHandSword_weapon() { }
    

}
