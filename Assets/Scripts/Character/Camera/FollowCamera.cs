﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] Transform _target;
    [SerializeField] private float _smooth = 0.1f;
    [SerializeField] Vector3 _velocity = Vector3.zero;

    private void Start() {
        _target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, _target.position, ref _velocity, _smooth);
    }
}
