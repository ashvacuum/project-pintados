﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats
{
    private float playerHP;
    private float playerSP;
    private float playerAS;
    private float playerDPS;

    private int randomCombo;
    //properties 
    public float PlayerHealth
    {
        get { return playerHP; }
        set { playerHP = value; }
    }
    public float PlayerStamina
    {
        get { return playerSP; }
        set { playerSP = value; }
    }
    public float PlayerAttackSpeed
    {
        get { return playerAS; }
        set { playerAS = value; }
    }
    public float PlayerDamagePerSecond
    {
        get { return playerDPS; }
        set { playerDPS = value; }
    }
    

     public int RandomCombo
    {
        get { return randomCombo; }
        set { randomCombo = value; }
    }

    //constructors
    public CharacterStats(){}

    public CharacterStats(float Player_HP, float Player_SP)
    {
        playerHP = Player_HP;
        playerSP = Player_SP;
    }
}
