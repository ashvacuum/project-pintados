﻿using RPG.Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Interaction {
    public class Interactor : MonoBehaviour {
        private IInteractable _currentInteractable = null;

        public PlayerInventory _inventory = null;

        private void Update() {
            CheckForInteraction();
        }

        private void CheckForInteraction() {
            if (_currentInteractable != null) {
                if (Input.GetKeyDown(KeyCode.E)) {
                    _currentInteractable.Interact();
                }
            }
        }

        private void OnTriggerEnter(Collider other) {
            if(other != null) {
                _currentInteractable = other.gameObject.GetComponent<IInteractable>();
            }
        }

        private void OnTriggerExit(Collider other) {
            var interactable = other.gameObject.GetComponent<IInteractable>();
            if(interactable == null) { return; }
            if(interactable == _currentInteractable) {
                _currentInteractable = null;
            }
        }
        
    }
}
