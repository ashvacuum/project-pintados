﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Interaction {
    public class TestInteractable : MonoBehaviour, IInteractable {

        public void Interact() {
            Debug.Log("Hello");
        }
    }
}
