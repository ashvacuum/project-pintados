﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Interaction {
    public interface IInteractable {
        void Interact();
    }
}
