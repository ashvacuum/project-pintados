﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyTargeting : MonoBehaviour
{
    public float checkRaidus;
    public LayerMask checkLayers;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, checkRaidus, checkLayers);
            Array.Sort(colliders, new TargetingSystem(transform));
            
            foreach(Collider item in colliders)
            {
                Debug.Log(item.name);
              
            }
            

            
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, checkRaidus);
    }
}
