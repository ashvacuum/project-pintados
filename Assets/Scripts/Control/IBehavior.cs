﻿
using UnityEngine;

namespace RPG.Behavior
{
    public interface IBehavior
    {
        void DoSomething();

        int DoNothing(int number);
    }
}