﻿using RPG.Behavior;
using UnityEngine;

public class En : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<IBehavior>().DoSomething();
    }
}
