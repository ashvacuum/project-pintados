﻿using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Core;


namespace RPG.Control
{
    public class PlayerController : MonoBehaviour
    {
        Mover mover;
        Health health;
        
        // Start is called before the first frame update
        void Start()
        {
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!health.IsAlive) return;
            if (InteractWithCombat()) { return; }
            if (InteractWithMovement()) { return; }
            //print("No more stuff here");
        }

        private bool InteractWithCombat()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            foreach (RaycastHit hit in hits)
            {
                TargetCombat combat = hit.transform.gameObject.GetComponent<TargetCombat>();
                if (combat!= null)
                {                    
                    if (!GetComponent<Fighter>().CanAttack(combat.gameObject)) continue;

                    if (Input.GetMouseButtonDown(0))
                    {
                        GetComponent<Fighter>().Attack(combat.gameObject);                        
                    }
                    return true;
                }
            }
            return false;
        }

        private bool InteractWithMovement()
        {
            bool hasHit = Physics.Raycast(GetMouseRay(), out RaycastHit hit, Mathf.Infinity);
            if (hasHit)
            {
                if (Input.GetMouseButton(0))
                {
                    mover.StartMoveAction(hit.point, 1f);
                }
                return true;
            }
            Debug.DrawRay(GetMouseRay().origin, GetMouseRay().direction * 100);
            return false;
        }

        private static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}
