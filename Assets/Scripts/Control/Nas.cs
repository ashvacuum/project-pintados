﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nas : Char
{
    public int chance;
    public string id;


    List<Nas> nas = new List<Nas>();
    protected override void Movement()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Nas ReturnANas()
    {
        int bottom = 0;
        int top = 0;
        int sum = 0;

        foreach(Nas item in nas)
        {
            sum += item.chance;
        }

        int random = Random.Range(0, sum);

        for(int i = 0; i < nas.Count; i++)
        {
            top += nas[i].chance;
            if(random >= bottom && random < top)
            {
                return nas[i];
            }
            bottom = top;
        }

        return null;
    }

    Nas[] DropNumberOfItems(int number)
    {
        Nas[] arrayOfItems = new Nas[number];

        for(int i = 0; i < number; i++)
        {
            arrayOfItems[i] = ReturnANas();
        }

        return arrayOfItems;
    }
}
