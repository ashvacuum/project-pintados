﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Doors : MonoBehaviour
{
    Animator anim;


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "NPC")
        {
            anim.SetBool("DoorOpen", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "NPC")
        {
            anim.SetBool("DoorOpen", false);
        }
    }
}
