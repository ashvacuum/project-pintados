﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Utilities {
    public class ToggleActiveWithKeyPress : MonoBehaviour {

        [SerializeField] private KeyCode _keyCode = KeyCode.None;
        [SerializeField] private GameObject _objectToToggle = null;
        private void Update() {
            if (Input.GetKeyDown(_keyCode)) {
                _objectToToggle.SetActive(!_objectToToggle.activeSelf);
            }
        }
    }
}
