﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RPG.SceneManagement{
    public class SceneController : MonoBehaviour
    {

        [SerializeField]private GameObject objectSpawnOnStart;
        [SerializeField] Vector3 spawnLocation;
        [SerializeField]private Image image;
        [SerializeField]private GameObject cover;

        public void Load(int buildScene) => StartCoroutine(LoadScene(buildScene));

        public void Exit() => Application.Quit();

        IEnumerator LoadScene(int buildScene){
            cover.SetActive(true);
            if(buildScene < 0){
                Debug.Log("incorrect build scene");
                yield break;
            }

            GameObject g = Instantiate(objectSpawnOnStart, Vector3.zero, Quaternion.identity);
            g.SetActive(false);
            g.transform.GetChild(0).position = spawnLocation;
            AsyncOperation ops = SceneManager.LoadSceneAsync(buildScene);
            ops.allowSceneActivation = false;
            while(ops.progress >= 0.9f){
                image.fillAmount = ops.progress;
                yield return null;
            }
            Debug.LogError("trying to load");
            g.SetActive(true);
            ops.allowSceneActivation = true;
            Destroy(gameObject);
        }
    }
}
