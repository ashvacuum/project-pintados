﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.SceneManagement{
    public class Fader : MonoBehaviour
    {
        [SerializeField]private CanvasGroup canvasG;
        
        void Start(){
            canvasG = transform.parent.GetComponent<CanvasGroup>();
        }

        IEnumerator FadeOutIn(){
            yield return FadeOut(3f);
            Debug.Log("faded out");
            yield return FadeIn(1f);
            Debug.Log("faded in");
        }
        public IEnumerator FadeOut(float seconds){

            while(canvasG.alpha < 1){
                canvasG.alpha += Time.deltaTime/seconds;
                yield return null;
            }
        }

        public IEnumerator FadeIn(float seconds){

            while(canvasG.alpha > 0){
                canvasG.alpha -= Time.deltaTime/seconds;
                yield return null;
            }
        }
    }
}