﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RPG.SceneManagement{
    public class Portal : MonoBehaviour
    {
        enum DestinationIdentifier{
            a, b, c, d, e,
        }

        [SerializeField] private DestinationIdentifier destination;
        [SerializeField] int buildScene = -1;
        [SerializeField] private Transform spawnPoint;

        [SerializeField] private float fadeOutTime = 1f;
        [SerializeField] private float fadeInTime = 2f;
        [SerializeField] private float fadeWaitTime = 0.5f;


        void OnTriggerEnter(Collider other){
            if(other.CompareTag("Player")){
                StartCoroutine(Transition());
            }
        }

        IEnumerator Transition(){

            if(buildScene < 0){
                Debug.Log("incorrect build scene");
                yield break;
            }
            DontDestroyOnLoad(gameObject);
            Fader fader = FindObjectOfType<Fader>();
            yield return fader.FadeOut(fadeOutTime);
            yield return SceneManager.LoadSceneAsync(buildScene);

            Portal otherPortal = GetOtherPortal();
            otherPortal.UpdatePlayer(otherPortal);

            yield return new WaitForSeconds(fadeWaitTime);
            yield return fader.FadeIn(fadeInTime);

            print("scene loaded");
            Destroy(gameObject);
            
        }

        Portal GetOtherPortal(){
            
            foreach(Portal portal in FindObjectsOfType<Portal>()){
                if(portal == this) continue; 
                if(portal.destination != destination) continue;

                return portal;

                
            }
            return null;
        }

        void UpdatePlayer(Portal otherPortal){
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.transform.position = otherPortal.spawnPoint.position;
            player.transform.rotation = otherPortal.spawnPoint.rotation;
        }
    }
}
