﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using RPG.Events;
using RPG.Loot;

namespace RPG.Core {
    public class Health : MonoBehaviour {
        [SerializeField] private EquippedItemValues _values = null;
        [SerializeField] float _maxHealth = 100f;
        [SerializeField] float _currentHealth = 100f;
        [SerializeField] private LootContainer _lootContainer;

        public Action<float> onHealthPctChanged;
        public VoidEvent onDeath;

        public EquippedItemValues valuesReadOnly { get { return _values; } }

        Animator anim;
        bool isAlive;

        ActionScheduler scheduler;
        public bool IsAlive { get { return isAlive; } }

        private void Awake() {
            _lootContainer = GetComponent<LootContainer>();
            isAlive = true;
            anim = GetComponent<Animator>();
            scheduler = GetComponent<ActionScheduler>();
        }

        /// <summary>
        /// Takes damage or adds life
        /// </summary>
        /// <param name="damage">Add a negative float to reduce health</param>
        public void TakeDamage(int damage) {
            _currentHealth = _values == null ? Mathf.Max(_currentHealth - damage, 0) : _currentHealth - Mathf.Max(1,_values.DamageReduction(damage));
            float curentHealthPct = (float)_currentHealth / (float)_maxHealth;
            onHealthPctChanged(curentHealthPct);
            if (_currentHealth <= 0 && IsAlive) {
                Die();
            }
        }

        public void Heal(int heal) {
            if (_currentHealth > 0) {
                _currentHealth = Mathf.Max(_currentHealth + heal, _maxHealth);
                float curentHealthPct = (float)_currentHealth / (float)_maxHealth;
                onHealthPctChanged(curentHealthPct);
            }
        }

        private void Die() {
            isAlive = false;
            anim.SetTrigger("die");
            if (_lootContainer != null) {
                _lootContainer.DropItems();
            }
            if (scheduler != null) {
                scheduler.CancelCurrentAction();
            }
        }
    }
}
