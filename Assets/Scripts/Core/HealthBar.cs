﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Core {
    public class HealthBar : UIElements {
        [SerializeField] private Health _health;
        [SerializeField]private Image fillImage;


        private void Awake() {
            _health.onHealthPctChanged += HandleHealthChanged;
        }

        private void HandleHealthChanged(float pct) {
            fillImage.fillAmount = pct;
        }
        
        private void OnDisable() {
            
        }

    }
}
