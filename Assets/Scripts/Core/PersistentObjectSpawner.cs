﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core{
    public class PersistentObjectSpawner : MonoBehaviour
    {
     

        static bool hasSpawned =false;

        private void Awake(){           

            SpawnPersistentObjects();
            
        }

        void SpawnPersistentObjects(){
            DontDestroyOnLoad(this);
        }
    }
}