﻿using RPG.Inventory.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core {
    [CreateAssetMenu(fileName = "Character Values", menuName = "Character Values/ New Character Value")]
    public class EquippedItemValues : ScriptableObject {

        [SerializeField] private bool _isPlayer = false;
        [SerializeField] private WeaponInventory _inventory;
        [SerializeField] private int _totalDamage = 0;
        [SerializeField] private int _totalArmor = 0;

        public bool isPlayer { get { return _isPlayer; } }

        public int DamageReduction(int damage) {
            int reduction = _inventory == null ? _totalArmor % 5 : _inventory.ReturnArmor() % 5;
            return damage - reduction;
        }

        public int TotalDamage() {
            int damage = _inventory == null ? _totalDamage :_inventory.ReturnDamage();
            return damage;
        }

    }
}
