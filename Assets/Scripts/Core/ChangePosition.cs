﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class ChangePosition : MonoBehaviour
    {
        [SerializeField] private Transform _locationToTeleport;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                other.gameObject.SetActive(false);
                other.transform.position = _locationToTeleport.position;
                other.gameObject.SetActive(true);
            }
        }
    }
}
