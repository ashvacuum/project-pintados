﻿using UnityEngine;
using System.Collections;

namespace RPG.Dialogue {
    [CreateAssetMenu(fileName = "New Character Dialogue", menuName = "Dialogue/New Character Dialogue")]
    public class Dialogue: ScriptableObject {
        [SerializeField]private string _name;
        [TextArea(3,10)]
        [SerializeField]private string[] _sentences;

        public new string name { get { return _name; } }
        public string[] sentences { get { return _sentences;  } }
    }
}
