﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Dialogue {
    public class DialogueManager : MonoBehaviour {

        [SerializeField] private TextMeshProUGUI _textDialogue;
        [SerializeField] private TextMeshProUGUI _textName;
        [SerializeField] private GameObject dialoguePanel;

        

        private Queue<string> _sentences;

        private void Start() {
            dialoguePanel.SetActive(false);
            _sentences = new Queue<string>();
        }

        public void StartDialogue(Dialogue dialogue) {
            dialoguePanel.SetActive(true);

            _textName.text = dialogue.name;

            _sentences.Clear();

            foreach(string sentence in dialogue.sentences) {
                _sentences.Enqueue(sentence);
            }

            DisplayNextSentence();
        }

        public void DisplayNextSentence() {
            if(_sentences.Count == 0) {
                EndDialogue();
                return;
            }
            string sentence = _sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
            
        }

        IEnumerator TypeSentence(string sentence) {
            _textDialogue.text = "";
            foreach (char letter in sentence.ToCharArray()) {
                _textDialogue.text += letter;
                yield return null;
            }
        }

        private void EndDialogue() {
            dialoguePanel.SetActive(false);
        }
    }
}
