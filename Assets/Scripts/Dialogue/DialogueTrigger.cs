﻿using UnityEngine;
using System.Collections;
using RPG.Interaction;

namespace RPG.Dialogue {
    public class DialogueTrigger : MonoBehaviour, IInteractable {

        [SerializeField] private Dialogue _dialogue;

        public void TriggerDialogue() {
            FindObjectOfType<DialogueManager>().StartDialogue(_dialogue);
        }

        public void Interact() {
            TriggerDialogue();
        }
    }
}
