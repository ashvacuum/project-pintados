﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Events {
    public class BoolListener : BaseGameEventListener<bool, BoolEvent, UnityBoolEvent> { }
}