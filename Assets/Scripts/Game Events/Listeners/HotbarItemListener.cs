﻿using RPG.Inventory;
using RPG.Inventory.Hotbar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Events {
    public class HotbarItemListener : BaseGameEventListener<Item, HotbarItemEvent, UnityHotbarItemEvent> { }
}