﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Events {
    public class IntListener : BaseGameEventListener<int, IntEvent, UnityIntEvent> { }
}