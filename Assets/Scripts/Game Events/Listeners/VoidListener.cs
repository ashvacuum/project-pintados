﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Events {
    public class VoidListener : BaseGameEventListener<Void, VoidEvent, UnityVoidEvent> { }
}