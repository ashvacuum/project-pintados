﻿using UnityEngine.Events;

namespace RPG.Events
{
    [System.Serializable]
    public class UnityIntEvent : UnityEvent<int> { }
   
}

