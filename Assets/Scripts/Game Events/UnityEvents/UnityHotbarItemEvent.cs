﻿using RPG.Inventory;
using RPG.Inventory.Hotbar;
using UnityEngine.Events;

namespace RPG.Events
{
    [System.Serializable]
    public class UnityHotbarItemEvent : UnityEvent<Item> { }
   
}

