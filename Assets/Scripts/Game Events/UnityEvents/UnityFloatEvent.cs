﻿using UnityEngine.Events;

namespace RPG.Events
{
    [System.Serializable]
    public class UnityFloatEvent : UnityEvent<float> { }
   
}

