﻿using UnityEngine.Events;

namespace RPG.Events
{
    [System.Serializable]
    public class UnityBoolEvent : UnityEvent<bool> { }
   
}

