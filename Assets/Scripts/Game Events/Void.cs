﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Events
{
    [System.Serializable]
    public struct Void {  }
}
