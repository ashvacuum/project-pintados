﻿using RPG.Inventory;
using RPG.Inventory.Hotbar;
using UnityEngine;

namespace RPG.Events
{
    [CreateAssetMenu(fileName = "New Hotbar Item Event", menuName = "Game Events/Hotbar Item Event")]
    public class HotbarItemEvent : BaseGameEvent<Item>
    {
        
    }
}
