﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Events
{
    [CreateAssetMenu(fileName = "New Float Event", menuName = "Game Events/Float Event")]
    public class FloatEvent : BaseGameEvent<float>
    {
     
    }
}
