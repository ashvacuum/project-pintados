﻿using System.Collections;
using System.Collections.Generic;
using RPG.Inventory.Weapons;
using UnityEngine;

namespace RPG.Combat {
    public class WeaponPrefabSwitcher : MonoBehaviour
    {
        [SerializeField] private GameObject _twoHand;
        [SerializeField] private GameObject _axe;
        [SerializeField] private GameObject[] _swordAndShield;

        public void WeaponSwitch(WeaponTypes type){
            switch(type){
                case WeaponTypes.Axe:
                    _axe.SetActive(true);
                    foreach(GameObject g in _swordAndShield){
                        g.SetActive(false);
                    }
                    _twoHand.SetActive(false);
                    break;

                case WeaponTypes.BareHanded:
                    _axe.SetActive(false);
                    foreach(GameObject g in _swordAndShield){
                        g.SetActive(false);
                    }
                    _twoHand.SetActive(false);
                    break;               

                case WeaponTypes.TwoHandSword:                    
                    _axe.SetActive(false);
                    _swordAndShield[0].SetActive(false);
                    _swordAndShield[1].SetActive(false);
                    _twoHand.SetActive(true);
                    break;

                case WeaponTypes.Sword:
                    if (!_twoHand.activeInHierarchy) {
                        _axe.SetActive(false);
                        _swordAndShield[0].SetActive(true);
                        _swordAndShield[1].SetActive(true);
                        _twoHand.SetActive(false);
                    }
                    break;
            }
        }
    }
}
