﻿using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat {
    public class HitEvents : MonoBehaviour {

        [SerializeField] private Health _playerValues;

        [SerializeField]private float _range = 2f;
        [SerializeField]private Transform originPoint;

        private void Awake() {
            _playerValues = GetComponent<Health>();
        }

        private void AnimationHit(int damage) {
            int damageCalculation = damage/2 + _playerValues.valuesReadOnly.TotalDamage();
            Debug.DrawRay(originPoint.position, transform.forward * _range, Color.red);
            RaycastHit[] straightRays = Physics.RaycastAll(originPoint.position, transform.forward, _range);
            RaycastHit[] SphereCastAll = Physics.SphereCastAll(originPoint.position, _range, transform.forward, _range);
            foreach(RaycastHit ray in SphereCastAll) {
                if(ray.collider.CompareTag("Player")) { continue; }
                if (ray.collider.GetComponent<Health>() != null) {
                    ray.collider.GetComponent<Health>().TakeDamage(damage);
                    Debug.Log(ray.collider.name);
                }
            }
        }
    }
}
